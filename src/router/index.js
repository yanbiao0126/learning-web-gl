import {createWebHashHistory, createRouter} from 'vue-router'

const routes = [
    {
        path: '/',
        component: () => import('../page/Home.vue')
    },
    {
        path: '/00',
        component: () => import('../page/00.vue')
    },
    {
        path: '/01',
        component: () => import('../page/01.vue')
    },
    {
        path: '/02',
        component: () => import('../page/02.vue')
    },
    {
        path: '/03',
        component: () => import('../page/03.vue')
    }
]

export default createRouter({
    history: createWebHashHistory(),
    routes
})